<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Robert Taylor | Web Developer</title>

    <meta name="HandheldFriendly" content="True">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Robert is a web developer based in Halifax, Nova Scotia">
    <meta name="robots" content="index, follow">

    <!-- Stylesheets -->

    <link rel="stylesheet" href="/assets/css/styles.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Heebo:900" rel="stylesheet">

</head>
<body>
<div class="site-wrap">
    <section class="c-main" id="home">
        <div class="c-main__intro">
            <nav>
                <a href="#home">Home</a>
                <a href="#about">About</a>
                <a href="#skills">Skills</a>
            </nav>
            <div class="c-main__wrapper container container--sm">
                <div class="c-main__logo">
                    <video loop autoplay muted playsinline>
                        <source src="/assets/video/blurred-lights.webm" type="video/webm">
                        <source src="/assets/video/blurred-lights.mp4" type="video/mp4">
                    </video>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 285 80" preserveAspectRatio="xMidYMid slice">
                        <defs>
                            <mask id="mask" x="0" y="0" width="100%" height="100%" >
                                <rect x="0" y="0" width="100%" height="100%" />
                                <text x="110"  y="68">R</text>
                            </mask>
                        </defs>
                        <rect x="0" y="0" width="100%" height="100%" />
                    </svg>
                </div>
                <div class="c-main__content">
                    <h1>I'm Robert, <br> a <span>web developer</span> based in Halifax, NS.</h1>
                </div>
            </div>
        </div>
        <div class="c-main__about" id="about">
            <div class="c-main__wrapper container">
                <div class="c-main__title">
                    <h2>about</h2>
                </div>
                <div class="c-main__content">
                    <p>I live in beautiful Halifax, Nova Scotia.</p>
                    <p>I'm available for both freelance and long term contract work. I build both small
                        and large scale websites, with an aim for performance and simplicity</p>
                    <p>Current interest: <span>React</span></p>
                    <p>Current binge: <span>E.R. and Modern Family</span></p>
                </div>
            </div>
        </div>
        <div class="c-main__skills" id="skills">
            <div class="c-main__wrapper container">
                <div class="c-main__title">
                    <h2>skills and experiences</h2>
                </div>
                <div class="c-main__content">
                    <p>I previously worked at <a href="http://www.verbinteractive.com" target="_blank">Verb Interactive</a>,
                        <br>an award winning digital agency,
                        based in Halifax, NS.</p>
                    <p>My focus is mainly front end, but I frequently work with both Wordpress and Drupal.</p>
                    <p>I've been member of teams that have built amazing websites, such as, <br>
                        <a href="https://www.nassauparadiseisland.com" target="_blank">Nassau Paradise Island</a>,
                        <a href="https://www.celebritycruises.com/edge/" target="_blank">Celebrity Cruises</a>,
                        <a href="https://www.cfib-fcei.ca/en" target="_blank">CFIB</a>, and
                        <a href="https://www.indianascoolnorth.com/" target="_blank">Indiana's Cool North</a>.
                    </p>
                    <div class="c-main__list">
                        <span>HTML</span>
                        <span>CSS/SCSS</span>
                        <span>JavaScript/Jquery</span>
                        <span>PHP</span>
                        <span>MySQL</span>
                        <span>Wordpress</span>
                        <span>Drupal</span>
                        <span>Git</span>
                        <span>Svn</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="c-main__contact">
            <div class="c-main__wrapper container">
                <h2>Looking to start a project? <a href="mailto:rcntaylor@gmail.com">Contact me</a></h2>
            </div>
        </div>
    </section>
</div>
</body>
</html>