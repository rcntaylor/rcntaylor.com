// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var sass = require('gulp-sass'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    pxtorem = require('postcss-pxtorem'),
    stylish = require('jshint-stylish'),
    notify = require('gulp-notify'),
    svgmin = require('gulp-svgmin'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin');

var config = {
    srcPath: 'src',
    distPath: 'assets',
    modulesPath: 'node_modules'
};

var handleErrors = function(errorObject, callback) {
    notify.onError(errorObject.toString().split(': ').join(':\n')).apply(this, arguments)
    // Keep gulp from hanging on this task
    if (typeof this.emit === 'function') this.emit('end')
};

var processors = [
    autoprefixer({
        browsers: ['last 3 versions', 'ios >= 7', 'IE >= 10']
    }),
    pxtorem({
        replace: true
    })
];

// Lint Task
gulp.task('lint', function() {
    return gulp.src(config.srcPath + '/js/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

// Compress SVGs & PNGs
gulp.task('svgs', function () {
    return gulp.src(config.srcPath + '/img/**/*.svg')
        .pipe(svgmin())
        .pipe(gulp.dest(config.distPath + '/img'));
});
gulp.task('pngs', function () {
    return gulp.src([config.srcPath + '/img/**/*.png', config.srcPath + '/img/**/*.jpg'])
        .pipe(imagemin())
        .pipe(gulp.dest(config.distPath + '/img'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src(config.srcPath + '/sass/**/*.scss')
        .pipe(sass({
            style: 'expanded',
            includePaths: [
                config.srcPath + '/sass',
                config.modulesPath + '/normalize-css/',
                config.modulesPath + '/sass-mq/'
            ]
        }))
        .on('error', handleErrors)
        .pipe(gulp.dest(config.distPath + '/css'))
});

// Run our post css tasks
gulp.task('css', ['sass'], function() {
    return gulp.src(config.distPath + '/css/**/*.css')
        .pipe(postcss(processors))
        // .pipe(cleanCSS())
        .pipe(gulp.dest(config.distPath + '/css'))
});

// Concatenate & Minify JS
gulp.task('scripts', ['lint'], function() {
    return gulp.src([
        /* Theme Scripts */
        // config.srcPath + '/js/global.js',
    ])
        .pipe(concat('main.js'))
        .pipe(gulp.dest(config.distPath + '/js'));
});


// Watch Files For Changes
gulp.task('default', function() {
    // Run build tasks
    gulp.start('svgs', 'pngs', 'css', 'scripts');

    gulp.watch(config.srcPath + '/**/*.svg', ['svgs', 'pngs']);
    gulp.watch(config.srcPath + '/**/*.js', ['scripts']);
    gulp.watch(config.srcPath + '/**/*.scss', ['css']);

});

// Default Task
gulp.task('build', function() {
    gulp.start('svgs', 'pngs', 'css', 'scripts');
});